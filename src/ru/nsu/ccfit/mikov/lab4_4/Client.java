package ru.nsu.ccfit.mikov.lab4_4;

import java.io.IOException;
import java.net.Socket;
import java.util.Timer;

import static ru.nsu.ccfit.mikov.lab4_4.Commons.*;

public class Client {

    private String address;
    private Socket socket;
    private int serverPort;
    private ChatView chatView;
    private int lastMessageId;
    private ClientSender clientSender = null;
    private ClientReceiver clientReceiver = null;
    private Timer timer;
    private boolean isLogin = false;

    public static void main(String[] args) {
        new Client(args[0], Integer.parseInt(args[1]));
    }

    private Client(final String addr, final int port) {
        address = addr;
        serverPort = port;
        chatView = new ChatView(this);
    }

    public int getLastMessageId() {
        return lastMessageId;
    }

    public void setLastMessageId(int lastMessageId) {
        this.lastMessageId = lastMessageId;
    }

    public void setClientToken(String clientToken) {
        clientSender.setToken(clientToken);
    }

    public void postLogin(final String userName) {
        try {
            makeConnection();
        } catch (IOException e) {
            chatView.updateChat("Server doesn't exist with such ip");
            closeSocket();
            return;
        }
        clientSender.work(KEY_POST_LOGIN, userName);
        clientReceiver.setLastMethod(KEY_POST_LOGIN);
    }

    public void postLogout() {
        try {
            makeConnection();
        } catch (IOException e) {
            chatView.updateChat("Server doesn't exist with such ip");
            closeSocket();
            return;
        }
        clientSender.work(KEY_POST_LOGOUT, null);
        clientReceiver.setLastMethod(KEY_POST_LOGOUT);
    }

    public void getOneUser(final int id) {
        try {
            makeConnection();
        } catch (IOException e) {
            chatView.updateChat("Server doesn't exist with such ip");
            closeSocket();
            return;
        }
        clientSender.work(KEY_GET_ONE_USER, String.valueOf(id));
        clientReceiver.setLastMethod(KEY_GET_ONE_USER);
    }

    public void getAllUsers() {
        try {
            makeConnection();
        } catch (IOException e) {
            chatView.updateChat("Server doesn't exist with such ip");
            closeSocket();
            return;
        }
        clientSender.work(KEY_GET_ALL_USERS, null);
        clientReceiver.setLastMethod(KEY_GET_ALL_USERS);
    }

    public void postMessage(final String message) {
        try {
            makeConnection();
        } catch (IOException e) {
            chatView.updateChat("Server doesn't exist with such ip");
            closeSocket();
            return;
        }
        clientSender.work(KEY_POST_MESSAGE, message);
        clientReceiver.setLastMethod(KEY_POST_MESSAGE);
    }

    void getMessages(final String numberOfMessage) {
        clientSender.work(KEY_GET_MESSAGES, numberOfMessage);
        clientReceiver.setLastMethod(KEY_GET_MESSAGES);
    }

    private void makeConnection() throws IOException {
        try {
            if (socket == null || socket.isClosed()) {
                socket = new Socket(address, serverPort);
                clientSender = new ClientSender(socket);
                clientReceiver = new ClientReceiver(socket, chatView, this);
                System.out.println("Client's created. Address: " + socket.getLocalAddress().getHostAddress()
                        + ":" + socket.getLocalPort());
                new Thread(clientSender).start();
                new Thread(clientReceiver).start();
                timer = new Timer();
                UpdateChatTask updateChatTask = new UpdateChatTask(this);
                timer.schedule(updateChatTask, 0, UPDATE_CHAT_PERIOD);
            }
        } catch (IOException e) {
            throw new IOException();
        }
    }

    public void closeSocket() {
        try {
            if (socket != null) {
                socket.close();
            }
            if (timer != null) {
                timer.cancel();
            }
            setLogin(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    boolean isLogin() {
        return isLogin;
    }

    void setLogin(boolean login) {
        isLogin = login;
    }
}