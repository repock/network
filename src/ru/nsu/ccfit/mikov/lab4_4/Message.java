package ru.nsu.ccfit.mikov.lab4_4;

public class Message {
    private int id;
    private String author;
    private String message;

    public int getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }

    public Message(final int id, final String author, final String message) {

        this.id = id;
        this.author = author;
        this.message = message;
    }
}

