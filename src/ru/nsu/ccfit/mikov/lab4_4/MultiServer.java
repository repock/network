package ru.nsu.ccfit.mikov.lab4_4;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

public class MultiServer extends Thread {

    private ServerSocket serverSocket;
    private ConcurrentHashMap<String, User> usersSet;
    private ConcurrentHashMap<Integer, Message> messageList;
    private int lastUserId;

    public static void main(final String[] args) {
        MultiServer s = new MultiServer();
        s.start();
    }

    private MultiServer() {
        try {
            lastUserId = 0;
            serverSocket = new ServerSocket(5555);
            int serverPort = serverSocket.getLocalPort();
            System.out.println("Server started in : " + InetAddress.getLocalHost().getHostAddress() + " " + serverPort);
            usersSet = new ConcurrentHashMap<>();
            messageList = new ConcurrentHashMap<>();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            while (true) {
                System.out.println("Server's waiting for new clients");
                Socket individualSocket = serverSocket.accept();
                System.out.println("Server gets new client");
                new IndividualServer(individualSocket, usersSet, messageList, lastUserId);
                lastUserId++;
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}
