package ru.nsu.ccfit.mikov.lab4_4;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Hashtable;

import static ru.nsu.ccfit.mikov.lab4_4.Commons.*;

public class ClientReceiver implements Runnable {

    private DataInputStream bufferedReader;
    private String responseStartLine;
    private Hashtable<String, String> responseHeaders;
    private JSONObject responseJsonObject;
    private ChatView chatView;
    private String lastMethod;
    private Client mainClient;

    void setLastMethod(String lastMethod) {
        this.lastMethod = lastMethod;
    }

    ClientReceiver(final Socket socket, final ChatView chatView, final Client client) {
        try {
            mainClient = client;
            this.chatView = chatView;
            bufferedReader = new DataInputStream(socket.getInputStream());
            responseHeaders = new Hashtable<>();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            int isEnd;
            while (true) {
                byte[] bytes = new byte[BUF_SIZE];
                isEnd = bufferedReader.read(bytes);
                if (isEnd < 0) {
                    chatView.updateChat("Server left");
                    chatView.successLogout();
                    break;
                }
                String httpRequest = new String(bytes, "UTF-8");
                parseHttp(httpRequest);
            }
        } catch (final IOException e) {
            chatView.updateChat("Server left");
            chatView.successLogout();
            try {
                bufferedReader.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

    }

    private void parseHttp(final String incomingMessage) {
        responseStartLine = "";
        responseHeaders.clear();
        responseJsonObject = null;
        String[] messageStrings = incomingMessage.split("\r\n");
        int numberOfStrings = messageStrings.length;
        int endOfRequestHeaders = 0;
        for (String currentString : messageStrings) {
            if ("".equals(currentString)) {
                break;
            }
            endOfRequestHeaders++;
        }
        setResponseStartLine(messageStrings[0]);
        for (int i = 1; i < endOfRequestHeaders; i++) {
            appendHeaderParameter(messageStrings[i]);
        }
        if (numberOfStrings != endOfRequestHeaders) {
            int endPoint = messageStrings[numberOfStrings - 1].indexOf("\u0000");
            String jsonString = messageStrings[numberOfStrings - 1].substring(0, endPoint);
            decodeToJsonRequestBody(jsonString);
        }
        responseHandle();
    }

    private void responseHandle() {
        if (responseStartLine.contains("200")) {
            switch (lastMethod) {
                case KEY_POST_LOGIN: {
                    mainClient.setLogin(true);
                    chatView.successLogin(responseJsonObject.get("token").toString());
                    showResponseMessage("Login");
                    break;
                }
                case KEY_POST_LOGOUT: {
                    chatView.successLogout();
                    mainClient.closeSocket();
                    showResponseMessage("Logout");
                    break;
                }
                case KEY_POST_MESSAGE: {
                    showResponseMessage("Your message");
                    break;
                }
                case KEY_GET_ONE_USER: {
                    showResponseMessage("Get one user");
                    break;
                }
                case KEY_GET_ALL_USERS: {
                    chatView.showUsers(responseJsonObject.toJSONString());
                    break;
                }
                case KEY_GET_MESSAGES: {
                    chatView.showMessages(responseJsonObject.toJSONString());
                }
            }
        } else {
            showResponseMessage("Warning");
        }
    }

    private void showResponseMessage(final String head) {
        chatView.updateChat("\n***************" + head + "***********************");
        chatView.updateChat(responseStartLine);
        for (String key : responseHeaders.keySet()) {
            chatView.updateChat(key + ":" + responseHeaders.get(key));
        }
        if (responseJsonObject != null) {
            chatView.updateChat(responseJsonObject.toString());
        }
    }

    private void setResponseStartLine(final String requestLine) {
        if (requestLine == null || requestLine.length() == 0) {
            System.out.println("Wrong StartLine");
        }
        responseStartLine = requestLine;
    }

    private void appendHeaderParameter(final String header) {
        String[] splitHeader = header.split(":");
        if (splitHeader.length != 2) {
            System.out.println("Wrong header");
            return;
        }
        responseHeaders.put(splitHeader[0], splitHeader[1]);
    }

    private void decodeToJsonRequestBody(final String requestBody) {
        if (requestBody.equals("")) {
            return;
        }
        JSONParser parser = new JSONParser();
        try {
            responseJsonObject = (JSONObject) parser.parse(requestBody);
        } catch (final ParseException e) {
            responseJsonObject = null;
        }
    }
}
