package ru.nsu.ccfit.mikov.lab4_4;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

import static ru.nsu.ccfit.mikov.lab4_4.Commons.BUF_SIZE;
import static ru.nsu.ccfit.mikov.lab4_4.Commons.TIMEOUT_PERIOD;

public class IndividualServer extends Thread {

    private DataInputStream individualServerInput;
    private DataOutputStream individualSocketOutput;
    private RequestHandler handler;
    private int lastUserId;

    IndividualServer(final Socket socket, final ConcurrentHashMap<String, User> users,
                     final ConcurrentHashMap<Integer, Message> messages, final int userId) {
        try {
            lastUserId = userId;
            socket.setSoTimeout(TIMEOUT_PERIOD);
            individualServerInput = new DataInputStream(socket.getInputStream());
            individualSocketOutput = new DataOutputStream(socket.getOutputStream());
            handler = new RequestHandler(users, messages);
            start();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        String httpRequest;
        int isEnd;
        while (true) {
            try {
                byte[] bytes = new byte[BUF_SIZE];
                isEnd = individualServerInput.read(bytes);
                if (isEnd < 0) {
                    break;
                }
                httpRequest = new String(bytes, "UTF-8");
                String response;
                try {
                    response = handler.handle(httpRequest, lastUserId);
                    individualSocketOutput.write(response.getBytes());
                } catch (RestException e) {
                    individualSocketOutput.write(e.getMessage().getBytes());
                } finally {
                    individualSocketOutput.flush();
                }
                if (!handler.isLogin()) {
                    try {
                        individualServerInput.close();
                        handler.cancelTimer();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    break;
                }
            } catch (IOException e) {
                try {
                    individualServerInput.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                if (handler.isLogin()) {
                    handler.TimeoutLogout();
                }
                break;
            }
        }
    }
}