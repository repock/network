package ru.nsu.ccfit.mikov.lab4_4;

public class Data {
    private String nameOfMethod;
    private String message;

    public Data(final String nameOfMethod, final String message) {
        this.nameOfMethod = nameOfMethod;
        this.message = message;
    }

    public String getNameOfMethod() {
        return nameOfMethod;
    }

    public String getMessage() {
        return message;
    }
}
