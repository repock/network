package ru.nsu.ccfit.mikov.lab4_4;

import java.util.TimerTask;

public class UpdateChatTask extends TimerTask {
    private Client client;

    public UpdateChatTask(Client client) {
        this.client = client;
    }

    @Override
    public void run() {
        if (client.isLogin()) {
            client.getMessages(String.valueOf(client.getLastMessageId()));
        }
    }
}
