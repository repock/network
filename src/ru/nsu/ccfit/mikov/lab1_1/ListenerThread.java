package ru.nsu.ccfit.mikov.lab1_1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.HashSet;
import java.util.Timer;
import java.util.TimerTask;

public class ListenerThread extends Thread implements AppCommons{
    private MulticastSocket rec_socket = null;
    private byte[] buf = new byte[size_of_buf];
    private String group_ip;
    private InetAddress groupInetAddress;
    private HashSet<String> ip_table = new HashSet<>();

    public ListenerThread(String ip){
        group_ip=ip;
    }
    public void run() {
        try {
            rec_socket = new MulticastSocket(default_port);
            groupInetAddress = InetAddress.getByName(group_ip);
            rec_socket.joinGroup(groupInetAddress);

            Timer myTimer = new Timer();
            myTimer.schedule(new MyTimerTask(ip_table), 1000, time_of_update_ip);

            DatagramPacket rec_packet = new DatagramPacket(buf, buf.length);
            while (true) {
                rec_socket.receive(rec_packet);
                String recieved_ip = new String(rec_packet.getAddress().toString());
                ip_table.add(recieved_ip);
                if ("end".equals(recieved_ip)) {
                    break;
                }
            }
            rec_socket.leaveGroup(groupInetAddress);
            rec_socket.close();
        } catch (IOException ex) {
            System.out.println("" + ex.getMessage());
            ex.printStackTrace();
        }
    }
}
class MyTimerTask extends TimerTask {
    private HashSet<String> old_ip_table = new HashSet<>();
    private HashSet<String> ip_table;

    public MyTimerTask(HashSet<String> ip_table) {
        this.ip_table = ip_table;
    }

    @Override
    public void run() {
        if (!old_ip_table.equals(ip_table))
        {
            old_ip_table = (HashSet) ip_table.clone();
            System.out.printf("Number of copies is " + old_ip_table.size() + "\n");
            for (String elements : old_ip_table) {
                System.out.printf(elements + "\n");
            }
            System.out.printf("***************************\n");
        }
        ip_table.clear();
    }
}