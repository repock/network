package ru.nsu.ccfit.mikov.lab1_1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class SenderThread extends Thread implements AppCommons {
    private byte [] buf=new byte[size_of_buf];
    String group_ip;

    public SenderThread(String ip){
        group_ip=ip;
    }
    public void run(){
        try {
            DatagramSocket send_socket = new DatagramSocket(default_port,InetAddress.getByName("127.0.0.10"));
            String message = InetAddress.getLocalHost().getHostAddress();
            buf = message.getBytes();
            DatagramPacket packet = new DatagramPacket(buf, buf.length, InetAddress.getByName(group_ip), default_port);
            while (true) {
                send_socket.send(packet);
                Thread.sleep(time_of_send);
            }
        }
        catch (IOException | InterruptedException e){
            System.out.println("" + e.getLocalizedMessage());
        }
    }
}