package ru.nsu.ccfit.mikov.lab1_1;

public class App extends Thread implements AppCommons {
    private SenderThread senderThread;
    private ListenerThread listenerThread;
    private static String group_ip;

    public App(String group_ip){
        this.group_ip=group_ip;
    }
    public void run() {
        senderThread = new SenderThread(group_ip);
        listenerThread=new ListenerThread(group_ip);
        listenerThread.start();
        senderThread.start();
    }

    public static void main(String[] args) {
        App app = new App(args[0]);
        app.start();
    }
}