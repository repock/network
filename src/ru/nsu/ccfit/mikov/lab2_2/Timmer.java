package ru.nsu.ccfit.mikov.lab2_2;


public class Timmer extends Thread {
    Speed lastSpeed = null;

    @Override
    public void run() {
        while (true) {
            try {
                outputLastRequestInfo();
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void outputLastRequestInfo() {
        System.out.print("TIMER: ");
        if (lastSpeed == null) {
            System.out.println("There were no requests");
        } else {
            System.out.println(lastSpeed);
        }
    }
}