package ru.nsu.ccfit.mikov.lab2_2;

public interface Commons {
    String SERVER_HOST = "127.0.0.1";
    int SERVER_PORT = 8080;
    int HEADER_BYTE_LENGTH = 20;
    int MAX_CONTENT_SIZE = 500 * 1024;
}