#include <iostream>
#include <utility>
#include <string> /* stoi */
#include <cstring> /* strlen */
#include <sys/types.h>
#include <sys/socket.h> /*gai_strerror*/
#include <netdb.h>
#include <vector>
#include <map>
#include <poll.h>
#include <assert.h>
#include <unistd.h> /* read, etc. */
#include <string.h> /* memcpy */
#include <functional>
#include <algorithm> /* remove_if */
#include <signal.h>
#include <fcntl.h>

const int BUFSIZE = 4096;
const int BASE_10 = 10;
const int TIMEOUT = -1;
int FDLIMIT = 253;

class Connection {
 public:
  Connection(int client, int target) {
    client_socket = client;
    target_socket = target;
  }

  ~Connection() {
    int code1 = ::close(client());
    int code2 = ::close(target());
    if (code1 == -1 || code2 == -1) {
      perror("close");
    }
  }

  bool done() {
    return (data_to_download == -1 && data_to_upload <= 0)
        || (data_to_download <= 0 && data_to_upload == -1);
  }

  short client_events() {
    return client_mask;
  }

  short target_events() {
    return target_mask;
  }

  int client() {
    return client_socket;
  }

  int target() {
    return target_socket;
  }

  bool uload_buf_empty() {
    return data_to_upload <= 0;
  }

  bool dload_buf_empty() {
    return data_to_download <= 0;
  }

  int read_from_client() {
    data_to_upload = read(client_socket, upload_buf, sizeof(upload_buf));
    if (data_to_upload > 0) {
      target_mask |= POLLOUT;
    } else if (data_to_upload == 0) {
      data_to_upload = -1;
    } else {
      perror("read from client");
    }
    client_mask ^= POLLIN;
    return data_to_upload;
  }

  int send_to_client() {
    assert(data_to_download > 0);
    int res = write(client_socket, download_buf, data_to_download);
    assert(res != 0);
    if (res == -1) {
      perror("send to client");
      data_to_download = -1;
    } else if (res != data_to_download) {
      char temp[BUFSIZE];
      memcpy(temp, download_buf, data_to_download - res);
      memset(download_buf, 0, BUFSIZE);
      memcpy(download_buf, temp, data_to_download - res);
      data_to_download -= res;
      return res;
    } else {
      data_to_download = 0;
    }
    client_mask ^= POLLOUT;
    target_mask |= POLLIN;
    return res;
  }

  int read_from_target() {
    data_to_download = read(target_socket, download_buf, sizeof(download_buf));
    if (data_to_download == -1) {
      perror("read_from_target");
    }
    if (data_to_download > 0) {
      client_mask |= POLLOUT;
    } else {
      data_to_download = -1;
    }
    target_mask ^= POLLIN;
    return data_to_download;
  }

  int send_to_target() {
    assert(data_to_upload > 0);
    int res = write(target_socket, upload_buf, data_to_upload);
    assert(res != 0);
    if (res == -1) {
      perror("send to target");
      data_to_upload = -1;
    } else if (res > data_to_upload) {
      char temp[BUFSIZE];
      memcpy(temp, upload_buf + res, data_to_upload - res);
      memset(upload_buf, 0, BUFSIZE);
      memcpy(upload_buf, temp, data_to_upload - res);
      data_to_upload -= res;
      std::cerr << "PARTIAL\n";
      return res;
    } else {
      data_to_upload = 0;
    }
    client_mask |= POLLIN;
    target_mask ^= POLLOUT;
    return res;
  }
  void invalidate() {
    std::cout << "invalidate()" << std::endl;
    data_to_upload = -1;
    data_to_download = -1;
    client_mask = 0;
    target_mask = 0;
  }

 private:
  ssize_t data_to_upload = 0;   // -1 means closed target
  ssize_t data_to_download = 0; // -1 means closed client
  short client_mask = POLLIN;
  short target_mask = POLLIN;
  int client_socket;
  int target_socket;
  char upload_buf[BUFSIZE];
  char download_buf[BUFSIZE];
};

class Poller {
 public:
  Poller(int listening_socket,
         std::function<int(void)> remote_socket_producer) {
    listen_socket = listening_socket;
    pollfds.push_back({listening_socket, POLLIN, 0});

    this->target_socket_producer = remote_socket_producer;
  }

  ~Poller() {
    std::for_each(pollfds.begin() + 1, pollfds.end(), [&](pollfd &fd) {
      Connection *conn = fd2conn[fd.fd];
      if (conn != nullptr) {
        fd2conn[conn->client()] = nullptr;
        fd2conn[conn->target()] = nullptr;
        delete conn;
      }
    });
  }

  void register_conn(Connection *conn) {
    pollfds.push_back({conn->client(), conn->client_events(), 0});
    pollfds.push_back({conn->target(), conn->target_events(), 0});
    fd2conn[conn->client()] = conn;
    fd2conn[conn->target()] = conn;
  }

  void run() {
    int nready;
    while (true) {
      nready = poll(pollfds.data(), pollfds.size(), TIMEOUT);
      assert(nready != 0);
      if (nready == -1) {
        perror("poll");
        exit(1);
      }
      for (int i = 0; i < pollfds.size(); ++i) {
        pollfd fd = pollfds[i];
        if (i == 0 && fd.revents & POLLIN) { // listening socket
          process_connect();
        }
        if (i != 0) {
          Connection *conn = fd2conn[fd.fd];
          process_event(fd, conn);
        }
      }
      drop_done_connections();
      update_masks();
    }
  }

 private:

  void drop_done_connections() {
    // Drop expired connections and its pollfds
    // none connection for listening socket at pollfds.begin()
    pollfds.erase(std::remove_if(pollfds.begin() + 1, pollfds.end(), [&](pollfd &fd) -> bool {
      Connection *conn = fd2conn[fd.fd];
      if (conn == nullptr) { // if conn deleted one step ago;
        return true;
      }
      if (conn->done()) {
        fd2conn[conn->client()] = nullptr;
        fd2conn[conn->target()] = nullptr;
        delete conn;
        return true;
      } else {
        return false;
      }
    }), pollfds.end());
   }

  void update_masks() {
    // keep fds in limit
    if (pollfds.size() < FDLIMIT) {
      pollfds[0].events = POLLIN;
    } else {
      pollfds[0].events = 0;
    }
    // none connection for listening socket at pollfds.begin()
    std::for_each(pollfds.begin() + 1, pollfds.end(), [&](pollfd &fd) {
      Connection *conn = fd2conn[fd.fd];
      assert(conn != nullptr);
      if (fd.fd == conn->client()) {
        fd.events = conn->client_events();
      } else {
        fd.events = conn->target_events();
      }
    });
  }

  void process_connect() {
    struct sockaddr client_addr;
    socklen_t addrlen = sizeof(client_addr);
    int client = accept(listen_socket, &client_addr, &addrlen);
    if (client == -1) {
      perror("accept");
      exit(1);
    }

    int code = fcntl(client, F_SETFL, fcntl(client, F_GETFL) | O_NONBLOCK);
    if (code == -1) {
      perror("fcntl");
      exit(1);
    }

    int target = target_socket_producer();
    if (target == -1) {
      close(client);
    } else {
      Connection *conn = new Connection(client, target);
      register_conn(conn);
    }
  }

  void process_event(pollfd fd, Connection *conn) {
    if (fd.revents & (POLLERR | POLLHUP | POLLNVAL)) {
      conn->invalidate();
    }
    if (fd.revents & POLLIN && fd.fd == conn->client() && conn->uload_buf_empty()) {
      conn->read_from_client();
    } else if (fd.revents & POLLIN && fd.fd == conn->target() && conn->dload_buf_empty()) {
      conn->read_from_target();
    } else if (fd.revents & POLLOUT && fd.fd == conn->client() && !conn->dload_buf_empty()) {
      conn->send_to_client();
    } else if (fd.revents & POLLOUT && fd.fd == conn->target() && !conn->uload_buf_empty()) {
      conn->send_to_target();
    }
  }

 private:
  int listen_socket;
  std::function<int(void)> target_socket_producer;
  std::vector<pollfd> pollfds;
  std::map<int, Connection *> fd2conn;
};

class Forwarder {
 public:
  Forwarder(std::string &&l_port, std::string &&t_host, std::string &&t_port) {
    this->listen_port = l_port;
    this->target_host = t_host;
    this->target_port = t_port;
    int code;

    struct addrinfo *hints = (struct addrinfo *) calloc(1, sizeof(struct addrinfo)); // new?
    hints->ai_socktype = SOCK_STREAM; // For TCP
    hints->ai_family = PF_INET; // For IPv4

    // Request target address info
    code = getaddrinfo(target_host.c_str(), target_port.c_str(),
                       hints, &target_addrinfo);
    if (code != 0) {
      std::cout << gai_strerror(code) << std::endl;
      exit(1);
    }

    // Request address info for listening socket
    hints->ai_flags = AI_PASSIVE; // to listen
    code = getaddrinfo(nullptr, listen_port.c_str(),
                       hints, &listen_addrinfo);
    if (code != 0) {
      std::cout << gai_strerror(code) << std::endl;
      exit(1);
    }

    listen_socket = socket(listen_addrinfo->ai_family,
                           listen_addrinfo->ai_socktype,
                           listen_addrinfo->ai_protocol);
    if (listen_socket == -1) {
      perror("socket");
      exit(1);
    }

    code = fcntl(listen_socket, F_SETFL, fcntl(listen_socket, F_GETFL) | O_NONBLOCK);
    if (code == -1) {
      perror("fcntl");
      exit(1);
    }

    if (bind(listen_socket, listen_addrinfo->ai_addr, listen_addrinfo->ai_addrlen)) {
      perror("bind");
      exit(1);
    }

    free(hints);
  }

  int create_target_socket() {
    int sock = ::socket(target_addrinfo->ai_family,
                        target_addrinfo->ai_socktype,
                        target_addrinfo->ai_protocol);
    if (sock < 0) {
      perror("sock");
      exit(1);
    }

    int code = fcntl(sock, F_SETFL, fcntl(sock, F_GETFL) | O_NONBLOCK);
    if (code == -1) {
      perror("fcntl");
      exit(1);
    }

    code = ::connect(sock, target_addrinfo->ai_addr, target_addrinfo->ai_addrlen);
    if (code == -1) {
      if (errno != EINPROGRESS) {
        perror("Connecting to server");
        return -1;
      }
    }
    return sock;
  }

  void listen() {
    if (::listen(listen_socket, FDLIMIT)) {
      perror("listen");
      exit(1);
    }

    Poller poller = Poller(listen_socket, [&]() { return create_target_socket(); });
    poller.run();
  }

  ~Forwarder() {
    freeaddrinfo(target_addrinfo);
    freeaddrinfo(listen_addrinfo);
    close(listen_socket);
   }

 private:

  struct addrinfo *target_addrinfo;
  struct addrinfo *listen_addrinfo;
  int listen_socket;
  std::string listen_port;
  std::string target_host;
  std::string target_port;
};

void exit_with_usage(std::string &&message) {
  std::cerr << "Usage: " << "./exec_name local_port remote_host remote_port" << std::endl;
  std::cerr << message << std::endl;
  exit(1);
}

void signal_handler(int code) {
  exit(0);
}

int main(int argc, char *argv[]) {

  if (argc != 4) {
    exit_with_usage(std::string("Bad args"));
  }

  FDLIMIT = sysconf(_SC_OPEN_MAX) - 5;
  if (FDLIMIT < 0) {
    perror("sysconf");
    exit(1);
  }

  signal(SIGINT, signal_handler);

  Forwarder forwarder = Forwarder(std::string(argv[1]), std::string(argv[2]), std::string(argv[3]));

  forwarder.listen();
  return 0;
}
