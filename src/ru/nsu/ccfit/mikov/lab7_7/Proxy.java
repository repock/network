package ru.nsu.ccfit.mikov.lab7_7;

import org.xbill.DNS.*;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;

public class Proxy implements Runnable {
    private final static int SIZE_OF_BUFFER = 131072;

    private Selector selector;
    private ServerSocketChannel serverSocketChannel;
    private SelectionKey selectionKey;

    public Proxy(int port) throws IOException {
        selector = Selector.open();
        serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(new InetSocketAddress("localhost", port));
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
    }

    @Override
    public void run() {
        try {
            DatagramChannel asClient = DatagramChannel.open();
            asClient.configureBlocking(false);

            selectionKey = asClient.register(selector, SelectionKey.OP_READ);
            selectionKey.attach(new DnsRequest());
            asClient.connect(new InetSocketAddress(ResolverConfig.getCurrentConfig().server(), 42));
        } catch (IOException e) {
            e.printStackTrace();
        }

        while(true) {
            int numberOfKeys = 0;
            try {
                numberOfKeys = selector.select();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(numberOfKeys == 0) {
                continue;
            }
            Set<SelectionKey> keys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = keys.iterator();

            while(iterator.hasNext()) {
                try {
                    SelectionKey key = iterator.next();

                    if(key.isAcceptable()) {
                        accept();
                    }
                    if(key.isReadable()) {
                        if(!read(key)) {
                            continue;
                        }
                    }
                    if(key.isWritable()) {
                        write(key);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            selector.selectedKeys().clear();
        }
    }

    private void accept() throws IOException {
        SocketChannel clientSocketChannel = serverSocketChannel.accept();
        clientSocketChannel.configureBlocking(false);
        SelectionKey clientKey = clientSocketChannel.register(selector, SelectionKey.OP_READ);

        Data data = new Data(clientKey, clientKey, true);
        clientKey.attach(data);
    }

    private boolean read(SelectionKey key) throws IOException {
        if(key.equals(selectionKey)) {
            return readFromDns(key);
        }

        SocketChannel channel = (SocketChannel) key.channel();
        Data data = (Data) key.attachment();
        ByteBuffer buffer = data.getBuffer();

        int bytesNumber = channel.read(buffer);
        if(bytesNumber == -1) {
            key.interestOpsAnd(0);
            return false;
        }

        if(data.isSetupMode()) {
            if(data.getStep() == 0) {
                firstStep(key, data);

            } else if(data.getStep() == 1) {
                step(key, data, buffer);
            }
            return true;
        } else {
            SelectionKey otherKey = key.equals(data.getServerKey()) ? data.getClientKey() : data.getServerKey();
            otherKey.interestOpsOr(SelectionKey.OP_WRITE);
            if(!buffer.hasRemaining()) {
                key.interestOpsAnd(~SelectionKey.OP_READ);
            }
            return true;
        }
    }

    private boolean readFromDns(SelectionKey dnsKey) throws IOException {
        DatagramChannel datagramChannel = (DatagramChannel) dnsKey.channel();
        DnsRequest dnsRequest = (DnsRequest) this.selectionKey.attachment();
        ByteBuffer buffer = ByteBuffer.allocate(SIZE_OF_BUFFER);

        datagramChannel.read(buffer);
        Message message = new Message(buffer.array());
        Record[] answers = message.getSectionArray(Section.ANSWER);
        if (0 != answers.length) {
            String ipAddress = answers[0].rdataToString();
            String domainName = answers[0].getName().toString();
            Data data = dnsRequest.getRequests().get(domainName);
            byte[] response = new byte[data.getDomainNameLength()];
            response[0] = 5;
            response[1] = 0;
            response[2] = 0;
            response[3] = 3;
            for(int i = 4; i < data.getDomainNameLength(); i++) {
                response[i] = data.getBuffer().array()[i];
            }
            dnsKey.interestOps(SelectionKey.OP_READ);

            finishSetup(data.getClientKey(), data, ipAddress, data.getServerPort(), response);
        }

        return true;
    }

    private void firstStep(SelectionKey key, Data data) {
        ByteBuffer buffer;
        byte[] response = new byte[2];
        response[0] = 5;
        response[1] = 0;
        buffer = ByteBuffer.wrap(response);
        data.setBuffer(buffer);

        data.increaseStep();
        key.interestOps(SelectionKey.OP_WRITE);
    }

    private void step(SelectionKey key, Data data, ByteBuffer buffer) throws IOException {
        if(checkBufferLength(buffer)){
            return;
        }

        if(buffer.array()[3] == 1) {
            setupIPv4(key, data, buffer);
        } else if(buffer.array()[3] == 3) {
            setupDomainName(key, data, buffer);
        } else {
            setupIPv6(key, data);
        }
    }

    private void setupIPv6(SelectionKey key, Data data) {
        ByteBuffer buffer;
        byte[] response = new byte[2];
        response[0] = 5;
        response[1] = 8;

        buffer = ByteBuffer.wrap(response);
        data.setBuffer(buffer);
        key.interestOps(SelectionKey.OP_WRITE);
    }

    private void setupDomainName(SelectionKey key, Data data, ByteBuffer buffer) {
        String domainName = extractDomainName(buffer);
        int port = extractPort(buffer, ((int) buffer.array()[4]) + 5, 2);

        data.setDomainNameLength((int) buffer.array()[4] + 5 + 2);
        data.setServerPort(port);
        DnsRequest dnsRequest = (DnsRequest) selectionKey.attachment();
        dnsRequest.getRequests().put(domainName + ".", data);
        dnsRequest.getToSend().add(domainName + ".");
        selectionKey.interestOps(SelectionKey.OP_WRITE);
        key.interestOpsAnd(0);
    }

    private void setupIPv4(SelectionKey key, Data data, ByteBuffer buffer) throws IOException {
        String ipAddress = extractIpAddress(buffer);
        int port = extractPort(buffer, 8, 2);

        byte[] response = new byte[10];
        response[0] = 5;
        response[1] = 0;
        response[2] = 0;
        response[3] = 1;
        for(int i = 4; i < 8; i++) {
            response[i] = buffer.array()[i];
        }
        for(int i = 8; i < 10; i++) {
            response[i] = buffer.array()[i];
        }
        finishSetup(key, data, ipAddress, port, response);
    }

    private boolean checkBufferLength(ByteBuffer buffer) {
        if(buffer.array().length < 3) {
            return true;
        }
        return false;
    }

    private void finishSetup(SelectionKey key, Data data, String ipAddress, int port, byte[] response) throws IOException {
        ByteBuffer buffer;
        buffer = ByteBuffer.wrap(response);
        data.setBuffer(buffer);
        data.increaseStep();
        key.interestOps(SelectionKey.OP_WRITE);

        SocketChannel serverSocketChannel = SocketChannel.open(new InetSocketAddress(ipAddress, port));
        serverSocketChannel.configureBlocking(false);
        SelectionKey serverKey = serverSocketChannel.register(selector, SelectionKey.OP_READ);

        data.setServerKey(serverKey);
        serverKey.attach(data);
        data.setReadyToReceiveData(true);
    }

    private int extractPort(ByteBuffer buffer, int offset, int length) {
        StringBuilder portBuilder = new StringBuilder();
        for(int i = offset; i < offset + length; i++) {
            String pl;
            if(buffer.array()[i] < 0) {
                pl = extraCode(buffer.array()[i]);
            } else {
                StringBuilder plBuilder = new StringBuilder(Integer.toString(buffer.array()[i], 2));
                while(plBuilder.length() < 8) {
                    plBuilder.insert(0, "0");
                }
                pl = plBuilder.toString();
            }
            portBuilder.append(pl);
        }
        return Integer.parseInt(portBuilder.toString(), 2);
    }

    private String extractIpAddress(ByteBuffer buffer) {
        StringBuilder ipAddress = new StringBuilder();
        for(int i = 4; i < 8; i++) {
            if(buffer.array()[i] < 0) {
                String extraCode = extraCode(buffer.array()[i]);
                int ipItem = Integer.parseInt(extraCode, 2);
                ipAddress.append(ipItem);
            } else {
                ipAddress.append(buffer.array()[i]);
            }
            if(i != 7) {
                ipAddress.append(".");
            }
        }
        return ipAddress.toString();
    }

    private String extractDomainName(ByteBuffer buffer) {
        StringBuilder domainName = new StringBuilder();
        for(int i = 5; i < buffer.array()[4] + 5; i++) {
            domainName.append((char) buffer.array()[i]);
        }
        return domainName.toString();
    }

    private String extraCode(int number) {
        int plusBin = number * (-1);
        plusBin -= 1;
        StringBuilder plBuilder = new StringBuilder(Integer.toString(plusBin, 2));
        while(plBuilder.length() < 8) {
            plBuilder.insert(0, "0");
        }
        String pl = plBuilder.toString();
        pl = pl.replace('0', '2');
        pl = pl.replace('1', '0');
        pl = pl.replace('2', '1');
        return pl;
    }

    private void write(SelectionKey key) throws IOException {
        if(key.equals(selectionKey)) {
            DatagramChannel datagramChannel = (DatagramChannel) key.channel();
            DnsRequest dnsRequest = (DnsRequest) selectionKey.attachment();
            dnsRequest.getToSend().forEach(domainName -> {
                try {
                    Message message = Message.newQuery(Record.newRecord(new Name(domainName), Type.A, DClass.ANY));
                    datagramChannel.write(ByteBuffer.wrap(message.toWire()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            dnsRequest.getToSend().clear();

            key.interestOps(SelectionKey.OP_READ);
            return;
        }

        SocketChannel channel = (SocketChannel) key.channel();
        Data data = (Data) key.attachment();
        ByteBuffer buffer = data.getBuffer();


        if(data.isSetupMode()) {

            channel.write(buffer);
            buffer.clear();
            data.setBuffer(ByteBuffer.allocate(SIZE_OF_BUFFER));

            if(data.isReadyToReceiveData()) {
                data.setSetupMode(false);
            }
            key.interestOps(SelectionKey.OP_READ);

        } else {
            SelectionKey otherKey = key.equals(data.getServerKey()) ? data.getClientKey() : data.getServerKey();
            buffer.flip();
            channel.write(buffer);
            buffer.flip();

            otherKey.interestOpsOr(SelectionKey.OP_READ);
            if(0 == buffer.position()) {
                key.interestOpsAnd(~SelectionKey.OP_WRITE);
            }
        }
    }
}
