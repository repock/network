package ru.nsu.ccfit.mikov.lab7_7;

import java.util.ArrayList;
import java.util.HashMap;

public class DnsRequest {
    private HashMap<String, Data> requests = new HashMap<>();
    private ArrayList<String> toSend = new ArrayList<>();

    public HashMap<String, Data> getRequests() {
        return requests;
    }

    public void setRequests(HashMap<String, Data> requests) {
        this.requests = requests;
    }


    public ArrayList<String> getToSend() {
        return toSend;
    }

    public void setToSend(ArrayList<String> toSend) {
        this.toSend = toSend;
    }
}
