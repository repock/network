package ru.nsu.ccfit.mikov.lab7_7;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            Proxy proxy = new Proxy(10088);
            proxy.run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
