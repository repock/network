package ru.nsu.ccfit.mikov.lab3_3;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Main {
    public static void main(String[] args) {
        if(args.length==3){
            String nodeName=args[0];
            int nodePercentLost=Integer.parseInt(args[1]);
            int nodePort=Integer.parseInt(args[2]);
            Node node=new Node(nodeName,nodePercentLost,nodePort);
            node.nodeCreate();

        }
        else if(args.length==5){
            String nodeName=args[0];
            int nodePercentLost=Integer.parseInt(args[1]);
            int nodePort=Integer.parseInt(args[2]);
            String nodeParentIp=args[3];
            int nodeParentPort=Integer.parseInt(args[4]);
            Node node;
            try {
                node = new Node(nodeName,nodePercentLost,nodePort,InetAddress.getByName(nodeParentIp),nodeParentPort);
                node.nodeCreate();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        else{
            System.out.println("Bad input, example for root nodeName,nodePercentLost,nodePort" +
                    " ,for rest nodeName,nodePercentLost,nodePort,nodeParentIp,nodeParentPort");
        }
    }
}