package ru.nsu.ccfit.mikov.lab3_3;

public final class Constants {
    public static final String CHILD_HEADER = "hello";
    public static final String CONFIRMATION_HEADER = "confirmation";
    public static final String MESSAGE_HEADER = "message";
    public static final int MIN_SIZE_PACKET = 3;

    public static final int RESENDING_PERIOD = 2000;
    public static final int NUMBER_OF_TRIES = 10;


}
