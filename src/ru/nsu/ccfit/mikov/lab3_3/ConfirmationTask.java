package ru.nsu.ccfit.mikov.lab3_3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static ru.nsu.ccfit.mikov.lab3_3.Constants.*;

public class ConfirmationTask extends TimerTask
{
    private ConcurrentHashMap<String, ConfirmationData> myUnconfirmed;
    private Set<String> myNeighbours;
    private ConcurrentHashMap<String, Date> myRecentReceived;
    private DatagramSocket myDatagramSocket;

    public ConfirmationTask(DatagramSocket datagramSocket, ConcurrentHashMap<String, ConfirmationData> unconfirmed
            ,ConcurrentHashMap<String, Date> recentReceived,Set<String> neighbours){
        myDatagramSocket=datagramSocket;
        myNeighbours= neighbours;
        myUnconfirmed = unconfirmed;
        myRecentReceived = recentReceived;
    }

    public void run()
    {
        //проходимся по всем неподтвержденным сообщениям
        for (Map.Entry<String, ConfirmationData> entry : myUnconfirmed.entrySet()) {
            if (entry.getValue().isEmpty())
                continue;

            Set<String> neighbours = entry.getValue().getMyNeighbours();
            byte[] buffer = (entry.getValue().getData()).getBytes();
            String prevSender=entry.getValue().getPrevSender();
            //повторно отсылаем это сообщение тем, кто не подтвердил его
            for (String address : neighbours) {
                if(address.equals(prevSender)){
                    entry.getValue().confirm(prevSender);
                    continue;
                }
                String[] receiverStringsAddress = address.split(":");
                InetSocketAddress receiverAddress = new InetSocketAddress(receiverStringsAddress[0]
                        , Integer.parseInt(receiverStringsAddress[1]));
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length, receiverAddress);
                try {
                    System.out.println("---Try to send "+entry.getKey()+" to "+address);
                    myDatagramSocket.send(packet);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //если число попыток равно 10, удаляем кто не подтвердил из соседей
            if (entry.getValue().getResendingCounter() == NUMBER_OF_TRIES)
            {
                neighbours=entry.getValue().getMyNeighbours();
                for (String address : neighbours) {
                    myNeighbours.remove(address);
                    myUnconfirmed.remove(entry.getKey());
                    System.out.println("Neighbour " + address + " left.");
                }
            }
        }
        //очищаем список только что полученных сообщений
        Date now = new Date();
        for (Map.Entry<String, Date> entry : myRecentReceived.entrySet())
        {
            if(now.compareTo(entry.getValue()) > RESENDING_PERIOD * NUMBER_OF_TRIES)
                myRecentReceived.remove(entry.getKey());
        }
    }
}