package ru.nsu.ccfit.mikov.lab3_3;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Date;
import java.util.Set;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;

import static ru.nsu.ccfit.mikov.lab3_3.Constants.RESENDING_PERIOD;

public class Node {
    private String myName;
    private InetAddress myParentIp;
    private int myParentPort;
    private int myPort;
    private int myPercentLost;
    private boolean isRoot;
    private DatagramSocket myDatagramSocket;

    public Node(String name,int percentLost, int port){
        myName =name;
        myPort=port;
        myPercentLost=percentLost;
        isRoot=true;
    }

    public Node(String name,int percentLost, int port, InetAddress parentIp,int parentPort){
        myName =name;
        myPort=port;
        myPercentLost=percentLost;
        myParentPort=parentPort;
        myParentIp=parentIp;
        isRoot=false;
    }

    public void nodeCreate(){
        Sender mySender;
        Receiver myReceiver;
        ConfirmationTask myConfirmationTask;
        ConcurrentHashMap<String, ConfirmationData> unconfirmed=new ConcurrentHashMap<>();
        ConcurrentHashMap<String, Date> recentReceived=new ConcurrentHashMap<>();
        Set<String> myNeighbours = ConcurrentHashMap.newKeySet();

        try {
            myDatagramSocket=new DatagramSocket(myPort);
        } catch (SocketException e) {
            e.printStackTrace();
        }

        if(isRoot) {
            mySender = new Sender(myName,myDatagramSocket,unconfirmed, myNeighbours);
        }
        else {
            mySender=new Sender(myName,myDatagramSocket,myParentIp,myParentPort,unconfirmed, myNeighbours);
        }

        myConfirmationTask=new ConfirmationTask(myDatagramSocket,unconfirmed,recentReceived, myNeighbours);
        myReceiver=new Receiver(myName,myDatagramSocket,myPercentLost, myNeighbours,recentReceived,unconfirmed,mySender);

        new Thread(mySender).start();
        new Thread(myReceiver).start();
        new Timer().schedule(myConfirmationTask,1000,RESENDING_PERIOD);
    }
}