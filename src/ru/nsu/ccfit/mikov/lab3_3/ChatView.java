package ru.nsu.ccfit.mikov.lab3_3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import static ru.nsu.ccfit.mikov.lab3_3.Constants.MESSAGE_HEADER;

class ChatView {
    private JTextArea messageChat;
    private JTextArea messageField;
    private Sender mySender;
    ChatView(Sender sender){
        mySender=sender;
        JFrame mainframe=new JFrame(sender.getMyName());

        mainframe.setSize(new Dimension(500,500));
        mainframe.setBackground(Color.gray);
        mainframe.setLayout(new GridLayout(3,1));

        JButton sendButton=new JButton("Send");
        sendButton.setFont(new Font("TimesRoman",Font.BOLD,30));
        sendButton.setFocusable(false);

        messageField=new JTextArea("");
        messageField.setFont(new Font("TimesRoman",Font.PLAIN,20));
        messageField.setLineWrap(true);
        messageField.requestFocus();

        messageChat=new JTextArea("");
        messageChat.setEditable(false);
        messageChat.setLineWrap(true);
        messageChat.setFont(new Font("TimesRoman",Font.PLAIN,20));
        messageChat.setFocusable(false);

        sendButton.addActionListener(e -> send());

        messageField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(KeyEvent.VK_ENTER==e.getKeyCode()){
                    send();
                    e.consume();
                }
            }
        });

        mainframe.add(new JScrollPane(messageChat));
        mainframe.add(new JScrollPane(messageField));
        mainframe.add(sendButton);

        mainframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainframe.setVisible(true);
    }

    void updateChat(String name,String msg){
        messageChat.append(name + ": " + msg);
        messageChat.append("\n");
        messageChat.setCaretPosition(messageChat.getDocument().getLength());
    }

    private void send(){
        String msg=messageField.getText();
        if(!"".equals(msg)) {
            mySender.putMessageToQueue(null,MESSAGE_HEADER,msg,null,null);
            updateChat(mySender.getMyName(),msg);
        }
        messageField.setText("");
    }
}