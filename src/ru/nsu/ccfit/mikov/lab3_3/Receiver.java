package ru.nsu.ccfit.mikov.lab3_3;

import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


import static java.nio.charset.StandardCharsets.UTF_8;
import static ru.nsu.ccfit.mikov.lab3_3.Constants.*;


public class Receiver implements Runnable {
    private DatagramSocket myDatagramSocket;
    private int myPercentLost;
    private String myName;
    private Set<String> myNeighbours;
    private ConcurrentHashMap<String, Date> myRecentReceived;
    private ConcurrentHashMap<String, ConfirmationData> myUnconfirmed;
    private Sender mySender;
    private ChatView myChat;

    public Receiver(String name,DatagramSocket datagramSocket,int percentLost,Set<String> neighbours
            , ConcurrentHashMap<String, Date> recentReceived
            , ConcurrentHashMap<String, ConfirmationData> unconfirmed, Sender sender){
        myPercentLost=percentLost;
        myName=name;
        myDatagramSocket=datagramSocket;
        myRecentReceived=recentReceived;
        myUnconfirmed =unconfirmed;
        myNeighbours=neighbours;
        mySender=sender;
        myChat=new ChatView(mySender);
    }

    private void receive(DatagramPacket myDatagramPacket){

        //декодируем байты в массив строк и определяем количество
        byte[] fullData = myDatagramPacket.getData();
        String[] messageStrings = decode (fullData);
        int numberOfStrings = messageStrings.length;

        //определяем кто послал текущее сообщение
        String senderAddress = myDatagramPacket.getAddress().getHostAddress();
        int senderPort = myDatagramPacket.getPort();
        String fullSenderAddress=senderAddress+":"+senderPort;

        //имитируем потерю пакета
        int randomPercentLost = new Random().nextInt(100);
        if (randomPercentLost < myPercentLost ) {
            System.out.println("***Lost message "+messageStrings[0]+" from "+fullSenderAddress);
            return;
        }

        if(numberOfStrings>MIN_SIZE_PACKET){
            //определеяем uuid собщения, header и его владельца
            String uuid=messageStrings[0];
            String messageHeader=messageStrings[1];
            String senderName=messageStrings[2];
            //определяем тип сообщения
            switch (messageHeader){
                case CHILD_HEADER:{
                    //получем текущий список соседей
                    if(!myNeighbours.contains(fullSenderAddress)){
                        myNeighbours.add(fullSenderAddress);
                        myChat.updateChat(myName,senderName + " " + fullSenderAddress + " connects.");
                    }
                    sendConfirmation(uuid,fullSenderAddress);
                    break;
                }
                case MESSAGE_HEADER:{
                    if(!myNeighbours.contains(fullSenderAddress)){
                        myNeighbours.add(fullSenderAddress);
                        myChat.updateChat(myName,senderName + " " + fullSenderAddress + " connects.");
                    }
                    if(myRecentReceived.get(uuid) == null) {
                        myRecentReceived.putIfAbsent(uuid,new Date());
                        //кладем полученное сообщение в очерель сообщений
                        mySender.putMessageToQueue(uuid, MESSAGE_HEADER, messageStrings[3], senderName, fullSenderAddress);

                        //передаем строки для отображения в UI
                        myChat.updateChat(senderName, messageStrings[3]);
                    }
                    sendConfirmation(uuid, fullSenderAddress);
                    break;
                }
                case CONFIRMATION_HEADER:{
                    myUnconfirmed.get(messageStrings[3]).confirm(fullSenderAddress);
                    break;
                }
                default:{
                    System.out.println("Something wrong in receiving");
                }
            }
        }

    }

    private void sendConfirmation(String uuid, String fullReceiverAddress)
    {
        Message myMessage=new Message(UUID.randomUUID().toString(),CONFIRMATION_HEADER,uuid, myName,null);
        byte[] buffer = myMessage.toString().getBytes();
        String[] receiverStringsAddress = fullReceiverAddress.split(":");
        InetSocketAddress receiverAddress = new InetSocketAddress(receiverStringsAddress[0], Integer.parseInt(receiverStringsAddress[1]));
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, receiverAddress);
        try {
            myDatagramSocket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

        while(true) {

            //считываем наш пакет в буфер
            byte[] buffer = new byte[4096];
            DatagramPacket myDatagramPacket = new DatagramPacket(buffer, 4096);
            try {
                myDatagramSocket.receive(myDatagramPacket);
            } catch (IOException e) {
                e.printStackTrace();
            }
            receive(myDatagramPacket);
        }
    }


    private String[] decode(byte[] encodedArray) {
        return new String(encodedArray, UTF_8).split("\n");
    }
}